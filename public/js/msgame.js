"use strict";
// Original game engine and sample code provided by Pavol Federl
// Game Engine: https://repl.it/@pfederl/Minesweeper-Game-Engine
// Lights Out Sample: https://gitlab.com/seng513/lights-out-game
//
// Modified by: Jesse Fowler
// Script requires JQuery

window.addEventListener('load', setup);

let MSGame = (function(){

  // private constants
  const STATE_HIDDEN = "hidden";
  const STATE_SHOWN = "shown";
  const STATE_MARKED = "marked";

  function array2d( nrows, ncols, val) {
    const res = [];
    for( let row = 0 ; row < nrows ; row ++) {
      res[row] = [];
      for( let col = 0 ; col < ncols ; col ++)
        res[row][col] = val(row,col);
    }
    return res;
  }

  // returns random integer in range [min, max]
  function rndInt(min, max) {
    [min,max] = [Math.ceil(min), Math.floor(max)]
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  class _MSGame {
    constructor() {
      this.difficulty(-1); // easy
    }
    validCoord(row, col) {
      // if exploded, no valid coords
      if( this.getStatus().done ) return false;
      return row >= 0 && row < this.nrows && col >= 0 && col < this.ncols;
    }
    init(nrows, ncols, nmines) {
      this.nrows = nrows;
      this.ncols = ncols;
      this.nmines = nmines;
      this.nmarked = 0;
      this.nuncovered = 0;
      this.exploded = false;
      // create an array
      this.arr = array2d(
        nrows, ncols,
        () => ({mine: false, state: STATE_HIDDEN, count: 0}));

      this.debug();
    }
    difficulty(difficulty){
      let diff = parseInt(difficulty);
      // Cheat = 5x7 grid - 1 mine
      // Easy = 8x10 grid - 10 mines
      // Medium = 14x18 - 40 mines
      // Hard = 20x24 - 100 mines
      let row = diff === 0 ? 8 :
                diff === 1 ? 14 :
                diff === -1 ? 5 :
                20;
      let col = diff === 0 ? 10 :
                diff === 1 ? 18 :
                diff === -1 ? 7 :
                24;
      let mns = diff === 0 ? 10 :
                diff === 1 ? 40 :
                diff === -1 ? 1 :
                100;
      this.init(row,col, mns);
    }
    count(row,col) {
      const c = (r,c) =>
            (this.validCoord(r,c) && this.arr[r][c].mine ? 1 : 0);
      let res = 0;
      for( let dr = -1 ; dr <= 1 ; dr ++ )
        for( let dc = -1 ; dc <= 1 ; dc ++ )
          res += c(row+dr,col+dc);
      return res;
    }

    sprinkleMines(row, col) {
      // ensure marked count gets reset
      this.nmarked = 0;
      // prepare a list of allowed coordinates for mine placement
      let allowed = [];
      for(let r = 0 ; r < this.nrows ; r ++ ) {
        for( let c = 0 ; c < this.ncols ; c ++ ) {
          if(Math.abs(row-r) > 2 || Math.abs(col-c) > 2)
            allowed.push([r,c]);
        }
      }
      this.nmines = Math.min(this.nmines, allowed.length);
      for( let i = 0 ; i < this.nmines ; i ++ ) {
        let j = rndInt(i, allowed.length-1);
        [allowed[i], allowed[j]] = [allowed[j], allowed[i]];
        let [r,c] = allowed[i];
        this.arr[r][c].mine = true;
      }
      // erase any marks (in case user placed them) and update counts
      for(let r = 0 ; r < this.nrows ; r ++ ) {
        for( let c = 0 ; c < this.ncols ; c ++ ) {
          if(this.arr[r][c].state == STATE_MARKED)
            this.arr[r][c].state = STATE_HIDDEN;
          this.arr[r][c].count = this.count(r,c);
        }
      }
      let mines = []; let counts = [];
      for(let row = 0 ; row < this.nrows ; row ++ ) {
        let s = "";
        for( let col = 0 ; col < this.ncols ; col ++ ) {
          s += this.arr[row][col].mine ? "B" : ".";
        }
        s += "  |  ";
        for( let col = 0 ; col < this.ncols ; col ++ ) {
          s += this.arr[row][col].count.toString();
        }
        mines[row] = s;
      }
      console.log("Mines and counts after sprinkling:");
      console.log(mines.join("\n"), "\n");
    }

    // uncovers a cell at a given coordinate
    // this is the 'left-click' functionality
    uncover(row, col) {
      console.log("uncover", row, col);
      // if coordinates invalid, refuse this request
      if( ! this.validCoord(row,col)) return false;

      // if this is the very first move, populate the mines, but make
      // sure the current cell does not get a mine
      if( this.nuncovered === 0)
        this.sprinkleMines(row, col);

      // if cell is not hidden, ignore this move
      if( this.arr[row][col].state !== STATE_HIDDEN ) return false;

      // floodfill all 0-count cells
      const ff = (r,c) => {
        if( ! this.validCoord(r,c)) return;
        if( this.arr[r][c].state !== STATE_HIDDEN) return;
        this.arr[r][c].state = STATE_SHOWN;
        this.nuncovered ++;
        if( this.arr[r][c].count !== 0) return;
        ff(r-1,c-1);ff(r-1,c);ff(r-1,c+1);
        ff(r  ,c-1);         ;ff(r  ,c+1);
        ff(r+1,c-1);ff(r+1,c);ff(r+1,c+1);
      };
      ff(row,col);
      // have we hit a mine?
      if( this.arr[row][col].mine) {
        this.exploded = true;
      }

      this.debug();
      return true;
    }

    // puts a flag on a cell
    // this is the 'right-click' or 'long-tap' functionality
    mark(row, col) {
      console.log("mark", row, col);
      // if coordinates invalid, refuse this request
      if( ! this.validCoord(row,col)) return false;
      // if cell already uncovered, refuse this
      console.log("marking previous state=", this.arr[row][col].state);
      if( this.arr[row][col].state === STATE_SHOWN) return false;
      // accept the move and flip the marked status
      this.nmarked += this.arr[row][col].state == STATE_MARKED ? -1 : 1;
      this.arr[row][col].state = this.arr[row][col].state == STATE_MARKED ?
        STATE_HIDDEN : STATE_MARKED;

      this.debug();
      return true;
    }

    // returns array of strings representing the rendering of the board
    //      "H" = hidden cell - no bomb
    //      "F" = hidden cell with a mark / flag
    //      "M" = uncovered mine (game should be over now)
    // '0'..'9' = number of mines in adjacent cells
    getRendering() {
      const res = [];
      for( let row = 0 ; row < this.nrows ; row ++) {
        let s = "";
        for( let col = 0 ; col < this.ncols ; col ++ ) {
          let a = this.arr[row][col];
          if( this.exploded && a.mine) s += "M";
          else if( a.state === STATE_HIDDEN) s += "H";
          else if( a.state === STATE_MARKED) s += "F";
          else if( a.mine) s += "M";
          else s += a.count.toString();
        }
        res[row] = s;
      }
      return res;
    }
    getStatus() {
      let done = this.exploded ||
          this.nuncovered === this.nrows * this.ncols - this.nmines &&
          this.nmarked === this.nmines;
      return {
        done: done,
        exploded: this.exploded,
        nrows: this.nrows,
        ncols: this.ncols,
        nmarked: this.nmarked,
        nuncovered: this.nuncovered,
        nmines: this.nmines
      }
    }

    debug() {
      let status = this.getStatus();
      $( ".grid-state" ).html( this.getRendering().join("<br>") );
      $( ".grid-status" ).html(
        "<h3>Status:</h3>" +
        "Done: " + status.done + "<br>" +
        "Exploded: " + status.exploded + "<br>" +
        "Rows: " + status.nrows + "<br>" +
        "Columns: " + status.ncols + "<br>" +
        "Mines: " + status.nmines + "<br>" +
        "Marked: " + status.nmarked + "<br>" +
        "Uncovered: " + status.nuncovered);
    }
  }

  return _MSGame;

})();

let game = new MSGame();

let stopwatch = null;

function setup() {
  // Setup play space and callbacks
  prepare_dom( game );
  // Display the play grid
  render( game );
  // Simulate selecting easy difficulty to start new game
  change_difficulty_cb(game, "0");
}

// Add game components to DOM and register callbacks
function prepare_dom(game_obj) {
  const grid = document.querySelector(".grid");
  const nTiles = 20 * 24 ; // max grid size

  // register callbacks for difficulty buttons
  document.querySelectorAll(".menuButton").forEach((button) =>{
      button.addEventListener("click", change_difficulty_cb.bind(null, game_obj, button.getAttribute("data-difficulty")));
  });

  // Set longtap trigger time
  $(document).bind("mobileinit", function () {
      $.event.special.tap.tapholdThreshold = 1000;
  });

  // Setup playspace tiles
  for( let i = 0 ; i < nTiles ; i ++) {
    const tile = document.createElement("div");
    const icon_container = document.createElement("span");
    tile.className = "tile";
    tile.setAttribute("data-tileInd", i);
    // Register callbacks for left and right mouse clicks
    $(tile).mousedown(function(event) {
        switch (event.which) {
            case 1:  // Left Mouse Click
                tile_click_cb( game_obj, tile, i);
                break;
            case 3:  // Right Mouse Click
                tile_rclick_cb( game_obj, tile, i);
                break;
            default:
                ;
        }
    });
    // Longtap for mobile right click
    $( tile ).bind( "taphold", function(event) {
        tile_rclick_cb(game_obj, tile, i);
    });

    tile.appendChild(icon_container);
    grid.appendChild(tile);
  }
}

// Render the gamespace area
function render(game_obj){
  // Setup tiles to be responsive in size with appropriate number of columns
  const grid = document.querySelector(".grid");
  let num_col_tiles = "";
  for(let i = 0; i < game_obj.getStatus().ncols; i++ ){
    num_col_tiles += "auto ";
  }
  grid.style.gridTemplateColumns = num_col_tiles;

  let tile_states = game.getRendering().join('');
  for( let i = 0 ; i < grid.children.length ; i ++) {
    const tile = grid.children[i];
    const ind = Number(tile.getAttribute("data-tileInd"));
    const col = ind % game_obj.getStatus().ncols;
    const row = Math.floor(ind / game_obj.getStatus().ncols);

    // Hide tiles that are outside the playspace
    if( ind >= game_obj.getStatus().nrows * game_obj.getStatus().ncols) {
      tile.style.display = "none";
    }

    // Checker pattern the tiles and format based on tile value:
    // H - Hidden (Green grass tile)
    // F - Flagged, a.k.a Marked tile (Mine Flag Icon)
    // M - Mine (Exploded Mine Icon)
    // 0:8 - Uncovered tile with mine proximity counts (Brown dirt tile)
    else {
      // Update data values
      tile.style.display = "block";
      tile.setAttribute("data-tilePattern", (col % 2 + row % 2) % 2 == 0 ? "even" :"odd");
      tile.setAttribute("data-tileValue", tile_states[ind]);

      tile.className = "tile ";
      tile.className += tile_states[ind] === "H" ? "covered" :
                        tile_states[ind] === "F" ? "marked" :
                        tile_states[ind] === "M" ? "mined" :
                        "uncovered";

      // Hidden Tiles and 0-Count Tiles Reset Style to be Empty
      if( tile_states[ind] === "H" || tile_states[ind] === "0"){
        tile.style.backgroundImage = "";
        tile.style.backgroundSize = "contain";
      }

      // Populate background SVG icons for numbers, flags and mines
      else {
        tile.style.backgroundImage = "url('media/" + tile_states[ind] + ".svg')";
        tile.style.backgroundSize = tile_states[ind] === "F" || tile_states[ind] === "M" ? "contain" : "contain"; //"2.5vw";
      }
    }
  }

  // Update Flag Counter
  document.getElementById("progress").innerHTML = "" + game_obj.getStatus().nmarked + "/" + game_obj.getStatus().nmines;

  // Check if End Game State
  if (game_obj.getStatus().done) {
    end_game(game_obj);
  }
}

// Timer Start and Stop functions
function stopwatch_start() {
  stopwatch_stop() // Ensure stopwatch is stopped before starting again
  $("#timer").text(0);
  stopwatch = setInterval(function(){
    let seconds = parseInt($("#timer").text()) + 1;
    $("#timer").text(seconds);
  }, 1000);
}
function stopwatch_stop() {
  clearInterval(stopwatch);
}

// Gamespace mouse-click and touch event callbacks
function tile_click_cb(game, tile, ind){
  const col = ind % game.getStatus().ncols;
  const row = Math.floor(ind / game.getStatus().ncols);
  game.uncover(row, col);
  render( game );

}
function tile_rclick_cb(game, tile, ind){
  const col = ind % game.getStatus().ncols;
  const row = Math.floor(ind / game.getStatus().ncols);
  game.mark(row, col);
  render( game );

}

// New game difficulty menu callback
function change_difficulty_cb(game, difficulty){
  let description = difficulty === "-1" ? "Cheat" :
                    difficulty === "0" ? "Easy" :
                    difficulty === "1" ? "Medium" :
                    "Hard";
  $("#difficultyMenu").text(description);
  $("#difficultyMenu").attr("data-value",difficulty);
  game.difficulty(difficulty);
  $("#progress").removeAttr("data-done");
  render( game );
  stopwatch_start();
}

// End game message display
function end_game(game_obj){
  // Bring up game-over screen
  stopwatch_stop();
  let message = "";
  if (game_obj.getStatus().exploded) {
    message += "You Lost!";
  } else {
    message += "You Won! " + game_obj.getStatus().nmines;
    message += game_obj.getStatus().nmines > 1 ? " Mines Cleared!" : " Mine Cleared!";
  }
  console.log(message);
  message += " Select a difficulty to start a new game";
  let difficulty =  $("#difficultyMenu").attr("data-value");
  $("#progress").html(message);
  $("#progress").attr("data-done", game_obj.getStatus().done);
}
